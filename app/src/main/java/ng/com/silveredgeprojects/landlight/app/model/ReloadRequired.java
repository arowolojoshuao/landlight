package ng.com.silveredgeprojects.landlight.app.model;

/**
 * Created by Matthew Odedoyin on 12/25/2017.
 */

public class ReloadRequired {
    public boolean reload;

    public ReloadRequired(boolean reload) {
        this.reload = reload;
    }
}
