package ng.com.silveredgeprojects.landlight.app.model.utilities;

/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */

public class ResponseModel{
    private String message;

    public ResponseModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
