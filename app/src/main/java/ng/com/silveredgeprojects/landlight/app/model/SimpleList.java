package ng.com.silveredgeprojects.landlight.app.model;

/**
 * Created by Matthew Odedoyin on 12/22/2017.
 */

public class SimpleList {
    private int icon;
    private String title;

    public SimpleList(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }


}
