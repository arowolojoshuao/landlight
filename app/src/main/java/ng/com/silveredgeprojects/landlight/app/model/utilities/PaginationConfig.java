package ng.com.silveredgeprojects.landlight.app.model.utilities;

public class PaginationConfig {
    private OrderByExpression orderByExpression;
    private int page =1;
    private int size =10;
    private WhereCondition whereCondition;

    public PaginationConfig(OrderByExpression orderByExpression, int page, int size, WhereCondition whereCondition) {
        this.orderByExpression = orderByExpression;
        this.page = page;
        this.size = size;
        this.whereCondition = whereCondition;
    }

    public OrderByExpression getOrderByExpression() {
        return orderByExpression;
    }

    public void setOrderByExpression(OrderByExpression orderByExpression) {
        this.orderByExpression = orderByExpression;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public WhereCondition getWhereCondition() {
        return whereCondition;
    }

    public void setWhereCondition(WhereCondition whereCondition) {
        this.whereCondition = whereCondition;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
