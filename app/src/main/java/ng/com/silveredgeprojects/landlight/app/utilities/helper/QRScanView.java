package ng.com.silveredgeprojects.landlight.app.utilities.helper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by BST-TECHNICAL on 5/26/2018.
 */

public class QRScanView extends View {
    public static Canvas mCanvas;
    Paint paint;
    Path path;
    PointF[] points;

    public QRScanView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
    }

    public void init(PointF[] points) {
        this.points = points;
        path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mCanvas = canvas;
        super.onDraw(mCanvas);
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setAntiAlias(true);
        path.moveTo(points[0].x , points[0].y);
        path.lineTo(points[1].x,points[1].y);
        path.lineTo(points[2].x,points[2].y);
        path.lineTo(points[0].x,points[0].y);
        path.close();
        canvas.drawPath(path,paint);

    }
}
