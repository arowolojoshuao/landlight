package ng.com.silveredgeprojects.landlight.app.model.interfaces;

/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */
public interface IOnPromiseCallBack {
    void onCacheResponse(String response, boolean updateInProgress, boolean isArray);

    void onSuccess(String response, boolean isArray);

    void onFailure(Throwable error);
}
