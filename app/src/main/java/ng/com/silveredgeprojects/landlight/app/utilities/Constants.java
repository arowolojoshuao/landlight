package ng.com.silveredgeprojects.landlight.app.utilities;


/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */

public class Constants {
    public static final int HTTP_POST = 1;
    public static final int HTTP_GET = 2;
    public static final int HTTP_DELETE = 3;
    public static final int HTTP_PUT = 4;
    public static final int VIEW_ITEM = 1;
    public static final int VIEW_PROG = 0;
    public static String waiting = "waiting";
    public static final String ISUSERLOGGEDIN = "isUserLoggedIn";
    public static String domain = "http://onevoice2016-001-site7.gtempurl.com/";
    public static String loginUrl = domain + "token";
    public static String baseUrl = domain + "api/";
    public static String contentType = "application/json";
    public static String token = "access_token";

    public static String TAG = "MMO";
    public static String dbContext = "MMO";
    public static int onlineMode = 1;
    public static int offlineMode = 0;
    public static String currentAppMode = "networkState";
    public static String appMode = "APP_MODE";
    public static String noOfflineData = "We didn't find anything to show here";
    public static final int HTTP_INTERNAL_SERVER_ERROR = 500;
    public static final int HTTP_BAD_REQUEST = 400;
    public static final int HTTP_AUTHORIZATION_ERROR = 401;
    public static final int HTTP_NOT_FOUND = 404;
    public static final int HTTP_OK = 200;
    public static String searchFragment = "searchFragment";
    public static String bundle = "bundle";
    public static String contentUpdated = "Content Updated";
    public static String contentUpdating = "Updating Content...";
    public static int LayerRequestCode = 200;
    public static int resultCode = 200;
    public static final String dispatch = "Dispatch";
    public static final String edit = "Edit";
    public static final String manageCorrespondence = "Manage Correspondence";
    public static final String receive = "Receive";
    public static final String details = "Details";
    public static final String returnAction = "Return";
    public static final String moveToDrawer = "Move to Drawer";
    public static String in = "IN";
    public static String out = "OUT";
    public static final String forward = "forward";
    public static String sessionExpired = "sessionExpired";
    public static int searchThresHold = 2;
    public static int defaultSize = 200;
    public static String requestBundle = "requestBundle";
//    public static int loadingView = R.layout.loading;
    public static String requestFolderBundle="requestFolderBundle";
    public static String currentUserId ="currentUserId";
    public static String timeOutError="Network error occurred. Check your internet settings";
    public static final int requestCode = 100;
    public static String QrCodeKey = "MMO";
    public static int SYNC_REQUEST_CODE = 3000;
    public static String syncTime="syncTime";
    public static String SYNC_CHANNEL="SYNC_CHANNEL";
    public static String RememberChoice = "RememberChoice";
    public static String isOfflineUser="isOfflineUser";
    public static final String synchronise = "Sync Request";
    public static final String trashRequest = "Trash request";
    public static String userName = "userName";
    public static String firstName = "FirstName";
    public static String lastName = "LastName";
}
