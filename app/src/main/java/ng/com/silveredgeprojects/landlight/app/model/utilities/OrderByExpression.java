package ng.com.silveredgeprojects.landlight.app.model.utilities;

public class OrderByExpression {
    private int column;
    private int direction;

    public OrderByExpression() {
        this.column=1;
        this.direction=1;
    }

    public OrderByExpression(int column, int direction) {
        this.column = column;
        this.direction = direction;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
