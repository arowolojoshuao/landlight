package ng.com.silveredgeprojects.landlight.app.utilities.helper;

import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by Matthew on 8/2/2016.
 */
public class ImageLoader {

    private final DisplayImageOptions options;
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader;

    public ImageLoader(int avatar) {
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageForEmptyUri(avatar)
                .showImageOnFail(avatar)
                .showImageOnLoading(avatar)
                .build();
    }

    public void displayImage(String url, ImageView imageView) {
        imageLoader.displayImage(url, imageView, options);
    }
}
