package ng.com.silveredgeprojects.landlight.app.utilities.helper;

import android.support.annotation.NonNull;
import android.text.method.PasswordTransformationMethod;
import android.view.View;


/**
 * Created by Matthew on 06/07/2016.
 */
public class AsteriskPasswordTransformation extends PasswordTransformationMethod {
    @Override
    public CharSequence getTransformation(CharSequence source, View view) {
        return new PasswordSequence(source);
    }
    class PasswordSequence implements CharSequence {
        private CharSequence mSource;

        public PasswordSequence(CharSequence mSource) {
            this.mSource = mSource;
        }

        @Override
        public int length() {
            return mSource.length();
        }

        @Override
        public char charAt(int index) {
            return '#';
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return mSource.subSequence(start,end);
        }

        @NonNull
        @Override
        public String toString() {
            return mSource.toString();
        }
    }
}

