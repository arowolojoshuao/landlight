package ng.com.silveredgeprojects.landlight.app.utilities.helper;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.exception.ConversionException;

import java.util.List;

import ng.com.silveredgeprojects.landlight.app.utilities.Utility;


/**
 * Created by Matthew on 1/9/2017.
 */

public class ValidationHelper implements Validator.ValidationListener {
    private Validator validator;
    private IValidationHelper iValidationHelper;
    private Context context;


    public ValidationHelper(Context context, IValidationHelper iValidationHelper, Validator validator) {
        this.validator = validator;
        this.iValidationHelper = iValidationHelper;
        this.context = context;
        init();
    }

    private void init() {
        validator.registerAdapter(TextInputLayout.class, new ViewDataAdapter<TextInputLayout, String>() {

            @Override
            public String getData(TextInputLayout view) throws ConversionException {
                return view.getEditText().getText().toString();
            }
        });
        validator.setValidationListener(this);
    }

    public void validate() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        iValidationHelper.onValidationSuccess();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);
            // Display error messages ;)
            if (view instanceof TextInputLayout) {
                setError((TextInputLayout) view, message);
            } else if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Utility.showNotification(view, message, Snackbar.LENGTH_LONG);
            }
        }
    }

    private void setError(TextInputLayout view, String message) {
        view.setErrorEnabled(true);
        view.setError(message);
    }

    public interface IValidationHelper {
        void onValidationSuccess();

    }

}
