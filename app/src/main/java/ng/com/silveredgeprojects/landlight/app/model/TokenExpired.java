package ng.com.silveredgeprojects.landlight.app.model;

/**
 * Created by Matthew Odedoyin on 12/25/2017.
 */

public class TokenExpired {
    public boolean isExpired;

    public TokenExpired(boolean isExpired) {
        this.isExpired = isExpired;
    }
}
