package ng.com.silveredgeprojects.landlight.app;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import ng.com.silveredgeprojects.landlight.app.model.User;


public class AppController extends Application {
    private static final int SCHEMA_VERSION = 0;

    private static final String TAG = "AppController";
    private Realm realmInstance;
    private User loginUser;

    public static AppController getInstance(Context context) {
        return (AppController) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("myrealm.realm")
                .schemaVersion(SCHEMA_VERSION)
                .deleteRealmIfMigrationNeeded().build();
           Realm.setDefaultConfiguration(config);
//        start();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }


    private void start(){
        Realm realm = getRealmInstance();
        User user = realm.where(User.class).findFirst();
        if (user != null) {
            setLoginUser(user);
        }
    }
    public Realm getRealmInstance() {
        return realmInstance;
    }

    public void setRealmInstance(Realm realmInstance) {
        this.realmInstance = realmInstance;
    }

    public User getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(User loginUser) {
        this.loginUser = loginUser;
    }




}
