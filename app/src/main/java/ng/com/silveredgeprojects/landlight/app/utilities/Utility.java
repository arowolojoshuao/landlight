package ng.com.silveredgeprojects.landlight.app.utilities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */

public class Utility {


    public static void configureToolbar(ActionBar actionBar, String title, boolean displayHomeButton) {
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(displayHomeButton);
    }

    public static String currentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return format.format(c);
    }

    public static String formatToServer(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return format.format(date);

    }


    public static String generateEncodedString(Bitmap bmp) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public static boolean isDeviceConnected(Context context) {
        boolean isDeviceConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworks = null;
        if (connectivityManager != null) {
            activeNetworks = connectivityManager.getActiveNetworkInfo();
        }
        if (null != activeNetworks) {
            if (activeNetworks.getType() == ConnectivityManager.TYPE_MOBILE) {
                isDeviceConnected = true;
            }
            if (activeNetworks.getType() == ConnectivityManager.TYPE_WIFI) {
                isDeviceConnected = true;
            }
        }
        return isDeviceConnected;
    }


    // https://stackoverflow.com/questions/9769554/how-to-convert-number-into-k-thousands-m-million-and-b-billion-suffix-in-jsp
    // Converts the number to K, M suffix
    // Ex: 5500 will be displayed as 5.5k
    public static String convertToSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.1f%c",
                count / Math.pow(1000, exp),
                "kmgtpe".charAt(exp - 1));
    }


    public static void logMessage(Object model) {
        Log.e(Constants.TAG, new Gson().toJson(model));
    }

    public static void customLogMessage(Object model) {
        Log.d(Constants.TAG, new Gson().toJson(model));
    }

    public static void showNotification(View view, String message, int length) {
        Snackbar snackbar = Snackbar.make(view, message, length);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView txtView = layout.findViewById(android.support.design.R.id.snackbar_text);
        txtView.setMaxLines(5);
        txtView.setTextColor(Color.parseColor("#FF4081"));
        snackbar.show();
    }


    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static View inflateView(Context context, int resource) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(resource, null);
    }


    public static String toLongDateString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        return format.format(date);

    }

    public static String formatDate(String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm aaa", Locale.getDefault());
        Date date = null;
        //check for the milliseconds part
        int position = dateFormat.lastIndexOf(".");
        if (position < 0) {
            dateFormat = dateFormat + ".000";
        }
        try {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
            date = serverFormat.parse(dateFormat);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return format.format(date);
    }

    public static String formatDbDate(String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm aaa", Locale.getDefault());
        Date date = null;
        try {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            date = serverFormat.parse(dateFormat);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return format.format(date);
    }

    private static int getColors(int index) {
        String[] colors = {"#469408", "#e69a2a", "#FF4081", "#414980", "#469408"};
        if (colors.length > (index - 1))
            return Color.parseColor(colors[index - 1]);
        return Color.parseColor(colors[0]);

    }

    public static void hideSoftKeyboard(Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }




    @Nullable
    private static Date getDate(String day) {
        SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = inFormat.parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getParsedData(String day) {
        Date date = getDate(day);
        SimpleDateFormat outFormat = new SimpleDateFormat("MMM d, yyyy");
        String goal = outFormat.format(date);

        return goal;
    }

//    public static void setStatusBackgroundColor(TextView txtView, int status) {
//        LayerDrawable drawable = (LayerDrawable) txtView.getBackground();
//        GradientDrawable gradientDrawable = (GradientDrawable) drawable.findDrawableByLayerId(R.id.statusShape);
//        gradientDrawable.setColor(getColors(status));
//    }


}
