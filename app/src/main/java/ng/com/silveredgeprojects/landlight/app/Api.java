package ng.com.silveredgeprojects.landlight.app;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by amento on 8/9/2018.
 */

public interface Api {

//    String BASE_SITE = "Your Base Site";
//
//    String BASE_URL = BASE_SITE + "Your Base Url";


    @FormUrlEncoded
    @POST("token")
    Call<ResponseBody> userLogin(
            @Field("username") String username,
            @Field("password") String password,
            @Field("grant_type") String grant_type
    );


    @POST("user/do_logout")
    Call<ResponseBody> logout(
            @Query("user_id") int user_id
    );


}
