package ng.com.silveredgeprojects.landlight.app.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.text.NumberFormat;
import java.util.Locale;

import ng.com.silveredgeprojects.landlight.app.dialog.Waiting;
import ng.com.silveredgeprojects.landlight.app.model.ReloadRequired;
import ng.com.silveredgeprojects.landlight.app.model.TokenExpired;
import ng.com.silveredgeprojects.landlight.app.model.User;
import ng.com.silveredgeprojects.landlight.app.model.utilities.OrderByExpression;
import ng.com.silveredgeprojects.landlight.app.model.utilities.PaginationConfig;
import ng.com.silveredgeprojects.landlight.app.model.utilities.WhereCondition;
import ng.com.silveredgeprojects.landlight.app.utilities.Constants;
import ng.com.silveredgeprojects.landlight.app.utilities.Utility;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */

@SuppressLint("Registered")
public class BaseCompatActivity extends AppCompatActivity {

    private Waiting wait;
    public static PaginationConfig paginationConfig;
    private Menu menu;

    private User request;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        if (getClass().getSimpleName().equalsIgnoreCase("DispatcherMainActivity"))
//            checkForConnectivity();
        paginationConfig = new PaginationConfig(new OrderByExpression(1, 1), 1, 10, new WhereCondition());

    }

    private void initTest() {
    }

//    private void checkForConnectivity() {
//        //TODO do this only on resume
//        // Utility.showNotification(findViewById(R.id.baseView), "Connecting...", Snackbar.LENGTH_INDEFINITE);
//        new Connectivity(this, isConnected -> {
//            if (isConnected) {
//                 Utility.showNotification(findViewById(R.id.main_container), "Connected...", Snackbar.LENGTH_LONG);
//            } else {
//                Utility.showNotification(findViewById(R.id.main_container), "Can't connect to network...", Snackbar.LENGTH_LONG);
//            }
//
//        }).execute();
//    }

    public void showWaiting() {
        wait = new Waiting();
        wait.show(getSupportFragmentManager(), Constants.waiting);
    }

    public void hideWaiting() {
        if (wait != null)
            wait.dismiss();
    }


    public void showError(String message) {
        hideWaiting();
        Utility.showToast(getApplicationContext(), message);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onTokenExpired(TokenExpired expired) {
        startActivity(new Intent(this, MainActivity.class).putExtra(Constants.sessionExpired, true));
    }

    @Subscribe
    public void ReloadRequired(ReloadRequired reloadRequired) {
        if (reloadRequired.reload) {
//            processBadge(menu);
        }
    }
    public String amountAdapter(int amo) {
        return "N " + NumberFormat.getNumberInstance(Locale.ENGLISH).format(amo) + " ";
    }

}

