package ng.com.silveredgeprojects.landlight.app;


import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import ng.com.silveredgeprojects.landlight.BuildConfig;
import ng.com.silveredgeprojects.landlight.app.utilities.Constants;
import ng.com.silveredgeprojects.landlight.app.utilities.helper.SharedPrefManager;
import ng.com.silveredgeprojects.landlight.app.utils.BooleanSerializerDeserializer;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.util.concurrent.TimeUnit.*;


public class RetrofitClient {
    private static final String BASE_URL = "http://onevoice2016-001-site7.gtempurl.com/";
    private static RetrofitClient mInstance;
    private static Context mContext;
    private static BooleanSerializerDeserializer booleanSerializerDeserializer = new BooleanSerializerDeserializer();
    public static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .serializeNulls()
            .registerTypeAdapter(Boolean.class, booleanSerializerDeserializer)
            .registerTypeAdapter(boolean.class, booleanSerializerDeserializer)
            .create();

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(30, MINUTES)
            .writeTimeout(30, MINUTES)
            .readTimeout(30, MINUTES)
            .build();
    private Retrofit retrofit;


    private RetrofitClient() {
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        if (BuildConfig.DEBUG) {
            okhttpClientBuilder.addInterceptor(logging);
        }
        GsonBuilder builder = new GsonBuilder().setLenient().
                excludeFieldsWithModifiers();
        Gson gson = builder.create();
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        okhttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                String token = SharedPrefManager.getInstance().getPref(Constants.token);
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json")
                        .header("access_token", token)
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

    }


    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }
}
