package ng.com.silveredgeprojects.landlight.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.agrawalsuneet.dotsloader.loaders.TrailingCircularDotsLoader;

import butterknife.ButterKnife;
import ng.com.silveredgeprojects.landlight.R;
import ng.com.silveredgeprojects.landlight.app.utilities.Constants;
import ng.com.silveredgeprojects.landlight.app.utilities.RuntimePermissionHelper;
import ng.com.silveredgeprojects.landlight.app.utils.SharedPreferenceUtil;


public class SplashScreen extends AppCompatActivity implements RuntimePermissionHelper.permissionInterface {
    public static Activity activity;
    String token;
    RuntimePermissionHelper runtimePermissionHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);

        runtimePermissionHelper = new RuntimePermissionHelper(SplashScreen.this, this);
        activity = this;
        runtimePermissionHelper.requestLocationPermission(1);
//        String token = SharedPrefManager.getInstance().getPref(Constants.token);
        token = SharedPreferenceUtil.getStringValue(SplashScreen.this, Constants.token);

//        Utility.showToast(SplashScreen_Activity.this, token);
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(2 * 1000);
                    loader_animation();

                    if (SharedPreferenceUtil.getBoolen(SplashScreen.this, Constants.ISUSERLOGGEDIN) && (token != null)) {

                        sleep(1 * 1000);
                        loader_animation();
                        startActivity(new Intent(SplashScreen.this, MainActivity.class));


                    } else {

                        sleep(1 * 1000);
                        loader_animation();
                        startActivity(new Intent(SplashScreen.this, MainActivity.class));

                    }

                    finish();


                } catch (Exception e) {

                }
            }
        };
        thread.start();
    }

    public void loader_animation() {
        TrailingCircularDotsLoader trailingCircularDotsLoader = new TrailingCircularDotsLoader(
                this,
                8,
                ContextCompat.getColor(this, android.R.color.holo_green_light),
                100,
                5);
        trailingCircularDotsLoader.setAnimDuration(900);
        trailingCircularDotsLoader.setAnimDelay(100);


    }


    @Override
    public void onSuccessPermission(int code) {

    }

}
