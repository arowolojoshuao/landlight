package ng.com.silveredgeprojects.landlight.app.model.utilities;

public class WhereCondition {
    public String searchBy;
    public int statusId;
    public int layerId;
    public int cabinetId;
    public int drawerId;
}
