package ng.com.silveredgeprojects.landlight.app.model.interfaces;

/**
 * Created by SilverEdgeProjects-O on 10/8/2017.
 */

public interface IConnectivity {
    void onCompleted(Boolean isConnected);
}
