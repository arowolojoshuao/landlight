package ng.com.silveredgeprojects.landlight.app.model.utilities;

/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */

public class ResourceObject<T> {
    private String url;
    private int method;
    private boolean isArray;
    private T data;


    public ResourceObject(String url, int method, boolean isArray) {
        this.url = url;
        this.method = method;
        this.isArray = isArray;
    }

    public ResourceObject(String url, int method, T data) {
        this.url = url;
        this.method = method;
        this.data = data;
    }

    public ResourceObject(String url, int method) {

        this.url = url;
        this.method = method;
        this.isArray = false;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public int getMethod() {
        return method;
    }

    public boolean isArray() {
        return isArray;
    }

    public T getData() {
        return data;
    }
}
