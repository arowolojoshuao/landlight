package ng.com.silveredgeprojects.landlight.app.utilities.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import javax.inject.Inject;


/**
 * Created by SilverEdgeProjects-O on 10/6/2017.
 */

@SuppressWarnings("WeakerAccess")
public class SharedPrefManager {

    private static SharedPrefManager mInstance;
    private static Context context;
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    @Inject
    SharedPreferences sharedPreferences;

    private SharedPrefManager(Context mContext) {
        context = mContext;
//        AppController.getStorageComponent().inject(this);
    }

    public static synchronized SharedPrefManager getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public boolean savePref(String key, Object data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, new Gson().toJson(data));
        editor.apply();
        return true;
    }

    public boolean saveToken(String key, String data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, data);
        editor.apply();
        return true;
    }

    public boolean deletePref(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.clear();
        editor.apply();

        return true;

    }


    public String getPref(String key) {
        return sharedPreferences.getString(key, null);
    }

    public long getLong(String key) {
        return Long.parseLong(sharedPreferences.getString(key, "0"));
    }

    public int getIntPref(String key) {
        return Integer.parseInt(sharedPreferences.getString(key, "0"));
    }



}
